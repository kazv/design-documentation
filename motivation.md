
# Motivation behind The Kazv Project

## Element UI is unsatisfying

People around me have reported that Element (was Riot.im) is not intuitive enough,
and sometimes unnecessarily complicated.

## Matrix as a private communication tool, not a social network

We use Matrix as a substitute for non-free, insecure messaging apps, and privacy
in conversations is a thing that cannot be ignored. For us end-to-end encryption
is a key feature, not an optional add-on. And so is calling. Features such as spaces
and person or room discovery are nice to have, but not necessary. Idenitiy servers
encourage people to bind their account to maybe sensitive personal information
and thus is not privacy-friendly, so we see this as potentially deterimental.

Many other Matrix projects do not prioritize private communication features, so they
do not meet our goals.

For social networking, maybe the Fediverse is more suitable for it.

## Convergence

We want to make a convergent application, which has similar UI and the same functionality
across platforms.

See also principles.md .

## Value-oriented design

This is more for developers. Value-orientation allows us to keep a single source of truth
and reduces back-and-forth signal/slot connections. This helps to clear the logic of the program
and eases development.
