
# Kazv UI Principles

## Convergent UI must have every functionality equally accessible on every platform.

If a functionality is available on one platform, it must be available on every platform
that has supporting hardware. For example, if the mobile version supports taking photo
with a camera, the desktop version must also support it, providing the platform has
an available camera.

"Screen size" is not an excuse to not make a functionality
available to some platform. For a bad example, GitLab only display the contribution
graph when the window is wide enough. Ideally, if the screen is not wide enough,
the user should still at least have the option to view the graph, and scroll it back
and forth.

For any two platforms A, B that support one certain functionality, the absolute value of
the difference of the minimum number of gestures needed to access that functionality
must not be greater than one. Gestures include clicking, pressing, swiping, touching, etc.

## Visual feedback must include one that is around the area of action.

If an action (such as a gesture) gives out visual feedback, there must be visual feedback
around the area of action. For a bad example, Slack's "View thread" link will only
open the thread in a sidebar, and nothing shows up around the point of clicking.
Ideally, it should at least display a message like "Opened in sidebar -->" around
that link, or highlight the message that the View thread link is attached to.

## No popups unless necessarily needed.

Popups interrupt users. If not needed, do not show popups. When it may be desirable to
show a popup, allow the user to change it into other forms.

For a bad example, Element Web shows a popup when "a call is answered elsewhere."
A call being answered on another device, especially a verified one, should be already
known to the user, so notifying them with a popup is not justified, and is only annoying
for users.

## No hidden features.

Do not hide a feature. For a bad example, Element Web supports screen sharing in calls,
but there is no "Share screen" button in the UI. Instead, users have to Shift+Click the
Video call button.
